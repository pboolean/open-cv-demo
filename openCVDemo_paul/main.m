//
//  main.m
//  openCVDemo_paul
//
//  Created by Paul White on 2014-04-29.
//  Copyright (c) 2014 Paul is Cool. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "pauliscoolAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([pauliscoolAppDelegate class]));
    }
}
