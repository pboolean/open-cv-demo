//
//  pauliscoolAppDelegate.h
//  openCVDemo_paul
//
//  Created by Paul White on 2014-04-29.
//  Copyright (c) 2014 Paul is Cool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pauliscoolAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
