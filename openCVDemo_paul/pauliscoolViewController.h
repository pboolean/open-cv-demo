//
//  pauliscoolViewController.h
//  openCVDemo_paul
//
//  Created by Paul White on 2014-04-29.
//  Copyright (c) 2014 Paul is Cool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <opencv2/videoio/cap_ios.h>

@interface pauliscoolViewController : UIViewController <CvVideoCameraDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *button;
- (IBAction)actionStart:(id)sender;

@end
