//
//  pauliscoolViewController.m
//  openCVDemo_paul
//
//  Created by Paul White on 2014-04-29.
//  Copyright (c) 2014 Paul is Cool. All rights reserved.
//  some code borrowed from https://github.com/gaurub/OpenCVTest
//

#import "pauliscoolViewController.h"
using namespace cv; // needed for OpenCV

// set this to whichever file you want to use.
NSString* const kFaceCascadeName = @"haarcascade_frontalface_alt";

#ifdef __cplusplus
CascadeClassifier face_cascade;
#endif

@interface pauliscoolViewController ()
{
    CvVideoCamera *videoCamera;
}

@property(strong, nonatomic) CvVideoCamera *videoCamera;
@property BOOL running;

@end

@implementation pauliscoolViewController

@synthesize videoCamera;
@synthesize imageView;
@synthesize running;

#pragma mark - ViewController lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setupCamera];
    [self loadFaceCascade];
    
    
    [self.button setTitle:@"Start" forState:UIControlStateNormal];
    running = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Controls

- (IBAction)actionStart:(id)sender
{
    if (!running) {
        [videoCamera start];
        [self.button setTitle:@"Stop" forState:UIControlStateNormal];
    } else
    {
        [videoCamera stop];
        [self.button setTitle:@"Start" forState:UIControlStateNormal];
    }
    
    running = !running;
}

#pragma mark - OpenCV delegate functions

#ifdef __cplusplus
- (void)processImage:(Mat&)image;
{
    std::vector<cv::Rect> faces;
    double boneOffsetX;
    double boneOffsetY;
    Mat frame_gray;
    
    cvtColor(image, frame_gray, CV_BGRA2GRAY);
    equalizeHist(frame_gray, frame_gray);
    
    face_cascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, cv::Size(100, 100));
    
    for(unsigned int i = 0; i < faces.size(); ++i) {
//        rectangle(image, cv::Point(faces[i].x, faces[i].y),
//                  cv::Point(faces[i].x + faces[i].width, faces[i].y + faces[i].height),
//                  cv::Scalar(0,255,255));
        
        // offsets
        boneOffsetX = faces[i].width / 3.5;
        boneOffsetY = faces[i].width / 11;
        
        // forehead
        circle(image,
               cv::Point(faces[i].x + boneOffsetX, faces[i].y + boneOffsetY),
               faces[i].width/12,
               cv::Scalar(127,0,0));
        
        circle(image,
               cv::Point(faces[i].x + faces[i].width - boneOffsetX, faces[i].y + boneOffsetY),
               faces[i].width/12,
               cv::Scalar(127,0,0));
        
        circle(image,
               cv::Point(faces[i].x + faces[i].width / 2, faces[i].y + faces[i].height/3.5),
               faces[i].width / 12,
               cv::Scalar(127, 127, 0));
//        rectangle(image, cv::Point(faces[i].x, faces[i].y),
//                  cv::Point(faces[i].x + faces[i].width, faces[i].y + (faces[i].height/4)),
//                  cv::Scalar(127,0,0),
//                  14,
//                  8);
//        putText(image, "Forehead",
//                cv::Point((faces[i].x + faces[i].width)/2, (faces[i].y + faces[i].height/6)),
//                FONT_HERSHEY_PLAIN,
//                0.5,
//                cv::Scalar(0, 0, 0));
    }
}

#endif

#pragma mark - Helper functions

- (void)setupCamera
{
    videoCamera = [[CvVideoCamera alloc] initWithParentView:imageView];
    [videoCamera setDefaultAVCaptureDevicePosition:AVCaptureDevicePositionFront];
    [videoCamera setDefaultAVCaptureSessionPreset:AVCaptureSessionPreset352x288];
    [videoCamera setDefaultAVCaptureVideoOrientation:AVCaptureVideoOrientationPortrait];
    [videoCamera setDefaultFPS:30];
    [videoCamera setGrayscaleMode:NO];
    
    [videoCamera setDelegate:self];
}

- (void)loadFaceCascade
{
    NSString *faceCascadePath = [[NSBundle mainBundle] pathForResource:kFaceCascadeName
                                                                ofType:@"xml"];
    
#ifdef __cplusplus
    if(!face_cascade.load([faceCascadePath UTF8String])) {
        NSLog(@"Could not load face classifier!");
    }
#endif
}


@end
